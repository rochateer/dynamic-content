import { Paper } from '@mantine/core'
import React from 'react'
import classes from './CardPaper.module.css'

function CardPaper({children} : {children : React.ReactNode}) {
  return (
    <Paper shadow='xs' radius={0} className={classes.card_paper}>
        {children}
    </Paper>
  )
}

export default CardPaper