import { Box, Text } from '@mantine/core'
import React from 'react'

function CardPaperHeader({children} : {children : React.ReactNode}) {
  return (
    <Box mb={20}>
      {children}
    </Box>
  )
}

export default CardPaperHeader