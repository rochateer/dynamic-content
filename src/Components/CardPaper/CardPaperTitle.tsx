import { Box, Text } from '@mantine/core'
import React from 'react'

function CardPaperTitle({children} : {children : React.ReactNode}) {
  return (
      <Text size="xl" fw={700} tt="uppercase" c={'blue.6'}>{children}</Text>
  )
}

export default CardPaperTitle