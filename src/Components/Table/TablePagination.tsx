import { Box, Center, Pagination, Text } from '@mantine/core'
import React from 'react'

function TablePagination({totalData, limitData = 10, totalCurrentData = 0, activePage, onChange} : {limitData? : number, activePage : number, totalCurrentData : number, totalData : number, onChange : ((value: number) => void)}) {
    
    const TOTAL_DATA = Math.ceil(totalData/limitData);

    return (
        <>
            <Center mt={10}>
                <Box>
                    <Text size="xs" mb={5}>Showing {totalCurrentData} from {totalData} Data</Text>
                    <Pagination radius={2} total={TOTAL_DATA} value={activePage} onChange={onChange} />
                </Box>
            </Center>
        </>
    )
}

export default TablePagination