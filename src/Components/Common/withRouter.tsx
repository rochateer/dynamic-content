import React, { ComponentType, FC } from 'react';
import {
  useLocation,
  useNavigate,
  useParams,
} from 'react-router-dom';

interface WithRouterProps {
  router: {
    location: ReturnType<typeof useLocation>;
    navigate: ReturnType<typeof useNavigate>;
    params: ReturnType<typeof useParams>;
  };
}

function withRouter<P extends {}>(
  WrappedComponent: React.ComponentType<P & WithRouterProps>
): FC<P> {
  const ComponentWithRouterProp: FC<P> = (props) => {
    const location = useLocation();
    const navigate = useNavigate();
    const params = useParams();

    const routerProps = {
      location,
      navigate,
      params,
    };

    return <WrappedComponent {...props} router={routerProps} />;
  };

  return ComponentWithRouterProp;
}

export default withRouter;
