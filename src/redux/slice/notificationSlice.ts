import { randomId } from '@mantine/hooks';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// const NOTIFY_FLAG_TEST = [
//   {id : "1", status : 'default', message : 'Welcome Back! This is a Toast Notification' },
//   {id : "2", status : 'error', message : 'Error ! An error occurred.' },
//   {id : "3", status : 'error', message : 'Error 2 An error occurred.' },
//   {id : "4", status : 'success', message : 'Your action was successful' },
// ]

interface Notification {
  id?: string;
  status : string;
  title?: string;
  message: string;
}

interface NotificationsState {
  notifications: Notification[];
}

const initialState: NotificationsState = {
  notifications: [],
};

const notificationsSlice = createSlice({
  name: 'notifications',
  initialState,
  reducers: {
    addNotification: (state, action: PayloadAction<Notification>) => {
      const payloadNotif = {...action.payload};
      payloadNotif['id'] = randomId().toString();
      state.notifications.push(action.payload);
    },
    removeNotification: (state, action: PayloadAction<string>) => {
      console.log("action.payload", action.payload)
      state.notifications = state.notifications.filter((notification) => notification.id !== action.payload);
    },
  },
});

export const { addNotification, removeNotification } = notificationsSlice.actions;

export default notificationsSlice.reducer;
