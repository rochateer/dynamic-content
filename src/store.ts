// src/app/store.ts
import { configureStore, combineReducers } from '@reduxjs/toolkit';
import notificationReducers from './redux/slice/notificationSlice';

const rootReducer = combineReducers({
  notifications: notificationReducers,
});

export const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
