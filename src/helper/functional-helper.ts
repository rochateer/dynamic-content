const generateRandomId = (lengthRandomId : number = 0) : string => {
    
    const lengthNew = 15 - lengthRandomId;
    
    return Math.random().toString(16).slice(lengthNew);
}

export const generateUniqueId = () : string => {

    const newUniqueId = generateRandomId(8)+"-"+generateRandomId(4)+"-"+generateRandomId(4)

    return newUniqueId
}