import { Autocomplete, NumberInput, TextInput, Textarea } from "@mantine/core";

const ComponentBuilder = {
    TextInput : TextInput,
    NumberInput : NumberInput,
    DropdownInput : Autocomplete,
    TextArea : Textarea
}

export default ComponentBuilder