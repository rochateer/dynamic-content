import { IconAdjustments, IconCalendarStats, IconFileAnalytics, IconGauge, IconLock, IconLogin, IconNotes, IconPresentationAnalytics, IconUsers } from "@tabler/icons-react";

export const MENU = [
    { label: 'Dashboard', icon: IconGauge },
    { label: 'Login', icon: IconLogin, link : '/login' },
    {
      label: 'User Management',
      icon: IconUsers,
      initiallyOpened: false,
      links: [
        { label: 'User', link: '/user' },
        { label: 'User Registration', link: '/user/create' },
      ],
    },
    {
      label: 'Market news',
      icon: IconNotes,
      initiallyOpened: false,
      links: [
        { label: 'Overview', link: '/' },
        { label: 'Forecasts', link: '/' },
        { label: 'Outlook', link: '/' },
        { label: 'Real time', link: '/' },
      ],
    },
    {
      label: 'Releases',
      icon: IconCalendarStats,
      links: [
        { label: 'Upcoming releases', link: '/' },
        { label: 'Previous releases', link: '/' },
        { label: 'Releases schedule', link: '/' },
      ],
    },
    { label: 'Analytics', icon: IconPresentationAnalytics },
    { label: 'Contracts', icon: IconFileAnalytics },
    { label: 'Settings', icon: IconAdjustments },
    {
      label: 'Security',
      icon: IconLock,
      links: [
        { label: 'Enable 2FA', link: '/' },
        { label: 'Change password', link: '/' },
        { label: 'Recovery codes', link: '/' },
      ],
    },
  ]