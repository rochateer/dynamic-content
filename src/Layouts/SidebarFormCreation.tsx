import React from 'react';
import { Box, Button, Grid, TextInput, Textarea } from '@mantine/core';
import { useForm } from '@mantine/form';
import { idxComponentDf } from '../@types/component-df';

function SidebarFormCreation({currentComponent} : {currentComponent : idxComponentDf}) {
  
  const form = useForm({
    initialValues: { field_label: currentComponent.field_label ? currentComponent.field_label : '', field_name: '', field_label_description: '' },

    // functions will be used to validate values at corresponding key
    validate: {
      field_label: (value : string) => (value.length < 2 ? 'Name must have at least 2 letters' : null),
      field_name: (value : string) => (value.length < 2 ? 'Name must have at least 2 letters' : null),
    },
  });

  // console.log("currentComponent.field_label", currentComponent.field_label)

  return (
    <>
        <Box pl={'15px'} pr={'15px'}>
        <form onSubmit={form.onSubmit(console.log)}>
          <Grid>
            <Grid.Col span={6}>
              <TextInput label="Field Label" placeholder="Field Label" name='field_label' value={currentComponent.field_label} {...form.getInputProps('field_label')}/>
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput label="Field Name" placeholder="Field Name" name='field_name' {...form.getInputProps('field_name')}/>
            </Grid.Col>
            <Grid.Col span={12}>
              <TextInput label="Field Placeholder" placeholder="Field Placeholder" name='field_placeholder' {...form.getInputProps('field_placeholder')}/>
            </Grid.Col>
            <Grid.Col span={12}>
              <Textarea label="Field Label Description" placeholder="Field Label Description" name='field_label_description' {...form.getInputProps('field_label_description')}/>
            </Grid.Col>
          </Grid>
          <Grid justify="flex-end" mt={'xs'}>
            <Grid.Col span={4}>
              <Button type="submit" size='xs'>Submit</Button>
            </Grid.Col>
          </Grid>
          </form>
        </Box>
    </>
  )
}

export default SidebarFormCreation