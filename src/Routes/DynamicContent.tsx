import DynamicContentPageCreationTest from '../pages/DynamicContentExample/DynamicContentPageCreationTest';

const dynamicContentRoutes = [
    {path : '/dynamic-content/creation', component: <DynamicContentPageCreationTest />},
];

export default dynamicContentRoutes