import {Navigate} from 'react-router-dom';
import UserManagement from '../pages/UserManagement';
import UserRegistration from '../pages/UserManagement/UserRegistration';

const userManagementRoutes = [
    {path : '/user', component: <UserManagement />},
    {path : '/user/create', component: <UserRegistration />},
];

export default userManagementRoutes