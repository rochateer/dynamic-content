import React from 'react'
import { Login } from "../pages/Authentication/Login";
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Dashboard from '../pages/Dashboard/Dashboard';
import Layout from '../Layouts';
import userManagementRoutes from './UserRegistration';
import dynamicContentRoutes from './DynamicContent';
import ComponentDynamicFormProvider from '../context/ComponentDynamicContentContext';

const publicRoutes = [
    {path : '/login', component: <Login />},
    {
      path: "/",
      component: <Navigate to="/dashboard" />,
    },
]

const protectedRoutes = [
  {path : '/dashboard', component: <Dashboard />},
  {
    path: "/",
    component: <Navigate to="/dashboard" />,
  },
]

const index = () => {
  return (
    <React.Fragment>
      <BrowserRouter>
      <Routes>
        {publicRoutes.map((route : { path: string; component: React.ReactElement}, idx : number) => 
          <Route
            path={route.path}
            element={
              route.component
            }
            key={idx}
          />
        )}
        {protectedRoutes.map((route : { path: string; component: React.ReactElement}, idx : number) => 
          <Route
            path={route.path}
            element={
              <Layout>{route.component}</Layout>
            }
            key={idx}
          />
        )}
        {userManagementRoutes.map((route : { path: string; component: React.ReactElement}, idx : number) => 
          <Route
            path={route.path}
            element={
              <Layout>{route.component}</Layout>
            }
            key={idx}
          />
        )}
        {dynamicContentRoutes.map((route : { path: string; component: React.ReactElement}, idx : number) => 
          <Route
            path={route.path}
            element={
              <ComponentDynamicFormProvider>
                <Layout type={'COMPONENT_CREATION'}>{route.component}</Layout>
              </ComponentDynamicFormProvider>
            }
            key={idx}
          />
        )}  
      </Routes>
      </BrowserRouter>
    </React.Fragment>
  )
}

export default index

