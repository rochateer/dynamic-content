import React from 'react'
import { ComponentDfContextType, currentActiveSectionSidebar, idxComponentDf } from '../@types/component-df';
import { generateUniqueId } from '../helper/functional-helper';

export const ComponentDynamicFormContext = React.createContext<ComponentDfContextType | null>(null);

const ComponentDynamicFormProvider : React.FC<{children : React.ReactNode}> = ({children}) => {
    
    const [componentDf, setComponentDf] = React.useState<idxComponentDf | null>(null);

    const [activeSectionSidebar, setActiveSectionSidebar] = React.useState<currentActiveSectionSidebar>('Menu');

    const addComponent = () => {
        // setComponentDf({id : generateUniqueId()})
        setActiveSectionSidebar("Form")
    }

    const selectComponent = (component_selected : idxComponentDf) => {
        setComponentDf(component_selected)
        setActiveSectionSidebar("Form")
    }

    const updateComponent = (component_selected : idxComponentDf) => {
        setComponentDf(component_selected)
    }

    // console.log("componentDf", componentDf)

    return <ComponentDynamicFormContext.Provider value={{ componentDf, addComponent, selectComponent, activeSectionSidebar, updateComponent }}>{children}</ComponentDynamicFormContext.Provider>
}

export default ComponentDynamicFormProvider


