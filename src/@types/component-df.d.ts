export interface idxComponentDf {
  id?: string;
  field_type?: string;
  field_label?: string;
  field_name?: string;
  field_placeholder?: string;
  field_description?: string;
  props?: any;
}

export type currentActiveSectionSidebar = 'Menu' | 'Form';

export type ComponentDfContextType = {
  componentDf: idxComponentDf | null;
  addComponent : (component : idxComponentDf) => void;
  selectComponent : (component_selected : idxComponentDf) => void;
  updateComponent : (component_selected : idxComponentDf) => void;
  activeSectionSidebar : currentActiveSectionSidebar
};