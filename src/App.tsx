import "@mantine/core/styles.css";
import { MantineProvider } from "@mantine/core";
import { theme } from "./theme";
import RouteSystem from "./Routes";
import './App.css'
import '@mantine/notifications/styles.css';
import { QueryClient, QueryClientProvider } from "react-query";
import { Notifications } from "@mantine/notifications";

export default function App() {

  const queryClient = new QueryClient();

  return (
    <MantineProvider theme={theme}>
      <Notifications position="top-right"/>
      <QueryClientProvider client={queryClient}>
        <RouteSystem />
      </QueryClientProvider>
    </MantineProvider>
  );
}
