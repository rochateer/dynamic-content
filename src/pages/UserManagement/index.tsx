import React from 'react'
import { CardPaper, CardPaperBody, CardPaperHeader, CardPaperTitle } from '../../Components/CardPaper'
import { Box, Table, Text } from '@mantine/core'
import { TablePagination } from '../../Components/Table';
import { useQuery } from 'react-query';

const fetchUsers = async (activePage : number = 1, limit : number = 10) => {
  const SKIP = (activePage-1)*limit
  const res = await fetch('https://dummyjson.com/users?limit='+limit+'&skip='+SKIP);
  return res.json();
};

function UserManagement() {

  const [activePage, setActivePage] = React.useState<number>(1);

  const {data, error, isLoading, isFetching} = useQuery({
    queryKey: ["usersList",activePage],
    queryFn: () => fetchUsers(activePage),
    keepPreviousData: true,
    staleTime: 10000,
  })

  const rows = data && data.users ? data.users.map((user : any) => (
    <Table.Tr key={user.id}>
      <Table.Td>{user.email}</Table.Td>
      <Table.Td>{user.firstName}</Table.Td>
      <Table.Td>{user.lastName}</Table.Td>
      <Table.Td>{user.username}</Table.Td>
      <Table.Td style={{textAlign : 'center'}}>{user.age}</Table.Td>
    </Table.Tr>
  )) : (
    <Table.Tr>
    </Table.Tr>
  );

  return (
    <React.Fragment>
      <CardPaper>
        <CardPaperHeader>
          <CardPaperTitle>User List</CardPaperTitle>
        </CardPaperHeader>
        <CardPaperBody>
          {isFetching && (
            <Box>
              <Text>Loading...</Text>
            </Box>
          )}
          
          <Table stickyHeader stickyHeaderOffset={60} withTableBorder highlightOnHover>
            <Table.Thead>
              <Table.Tr>
                <Table.Th>Email</Table.Th>
                <Table.Th>First Name</Table.Th>
                <Table.Th>Last Name</Table.Th>
                <Table.Th>Username</Table.Th>
                <Table.Th style={{textAlign : 'center'}}>Age</Table.Th>
              </Table.Tr>
            </Table.Thead>
            <Table.Tbody>
              {rows}
            </Table.Tbody>
            <Table.Caption>
              
              <TablePagination totalData={data && data.total} totalCurrentData={data?.users?.length} limitData={10} activePage={activePage} onChange={setActivePage} />
              {/* {elements.length} from {elements.length} data */}
            </Table.Caption>
          </Table>
        </CardPaperBody>
      </CardPaper>
    </React.Fragment>
  )
}

export default UserManagement