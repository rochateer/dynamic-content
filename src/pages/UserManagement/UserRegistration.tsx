import { Button, Grid, MultiSelect, NumberInput, Paper, SimpleGrid, TextInput } from '@mantine/core';
import { useForm } from '@mantine/form';
import React from 'react';
import { CardPaper, CardPaperBody, CardPaperHeader, CardPaperTitle } from '../../Components/CardPaper';

function UserRegistration() {

  const form = useForm({
    validateInputOnChange: true,
    initialValues: { name: '', email: '', age: 0, username : '', role : [] },

    // functions will be used to validate values at corresponding key
    validate: {
      name: (value) => (value.length < 2 ? 'Name must have at least 2 letters' : null),
      username: (value) => (value.length < 2 ? 'Name must have at least 2 letters' : null),
      email: (value) => (/^\S+@\S+$/.test(value) ? null : 'Invalid email'),
      age: (value) => (value < 18 ? 'You must be at least 18 to register' : null),
    },
  });

  return (
    <React.Fragment>
        <CardPaper>
          <CardPaperHeader>
            <CardPaperTitle>User Registration</CardPaperTitle>
          </CardPaperHeader>
            <CardPaperBody>
              <Grid>
                <Grid.Col span={8}>
                  <form onSubmit={form.onSubmit(console.log)}>
                    <TextInput mt={'xs'} label="Name" placeholder="Name" {...form.getInputProps('name')} />
                    <TextInput label="Email" placeholder="Email" {...form.getInputProps('email')} />
                    <TextInput label="Username" placeholder="Username" {...form.getInputProps('username')} />
                    <Grid>
                      <Grid.Col span={3}>
                        <NumberInput
                          label="Age"
                          placeholder="Age"
                          min={0}
                          max={120}
                          {...form.getInputProps('age')}
                        />
                      </Grid.Col>
                    </Grid>
                    <MultiSelect
                      data={[
                        { value: 'Admin', label: 'Admin' },
                        { value: 'Creator', label: 'Creator' },
                        { value: 'Viewer', label: 'Viewer' },
                        { value: 'API Admin', label: 'API Admin' },
                      ]}
                      {...form.getInputProps('role')}
                      label="Role"
                      placeholder="User Roles"
                      searchable
                    />
                    <Button type="submit" mt="xl">
                      Submit
                    </Button>
                  </form>
                </Grid.Col>
              </Grid>
            </CardPaperBody>
          </CardPaper>
    </React.Fragment>
  )
}

export default UserRegistration