import React from 'react';
import { Autocomplete, Box, Button, Grid, Modal, MultiSelect, NumberInput, Paper, SimpleGrid, TextInput } from '@mantine/core';
import { useForm } from '@mantine/form';
import { CardPaper, CardPaperBody, CardPaperHeader, CardPaperTitle } from '../../Components/CardPaper';
import { useDisclosure } from '@mantine/hooks';
import { ComponentDynamicFormContext } from '../../context/ComponentDynamicContentContext';
import { ComponentDfContextType, idxComponentDf } from '../../@types/component-df';
import { generateUniqueId } from '../../helper/functional-helper';

import './dynamic-content.css';

const ComponentForm : any= {
  TextInput : TextInput
}

const DynamicComponentItem = (
  { item, onClickSelect }
  :
  { item : idxComponentDf, onClickSelect : (id_component? : string) => void }
  ) => {
  const itemRef = React.useRef<HTMLDivElement | null>(null);

  const handleMouseEnter = () => {
    if (itemRef.current) {
      itemRef.current.classList.add("dynamic-content--selected-component");
    }
  };

  const handleMouseLeave = () => {
    if (itemRef.current) {
      itemRef.current.classList.remove("dynamic-content--selected-component");
    }
  };

  return (
    <Box
      key={item.id}
      onClick = {() => onClickSelect(item.id)}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      ref={itemRef}
    >
      {(item.field_type && item.id) && React.createElement(ComponentForm[item.field_type], {
          key : item.id, 
          mt :'xs', 
          label : item.field_label, 
          placeholder : item.field_placeholder,
          // ref:(ref => { elementRef.current[idx] = ref })
        }
      )}
    </Box>
  );
};

function DynamicContentPageCreationTest() {

  const { selectComponent } = React.useContext(ComponentDynamicFormContext) as ComponentDfContextType;

  const form = useForm({
    validateInputOnChange: true,
    initialValues: { name: '', email: '', age: 0, username : '', role : [] },

    // functions will be used to validate values at corresponding key
    validate: {
      name: (value) => (value.length < 2 ? 'Name must have at least 2 letters' : null),
      username: (value) => (value.length < 2 ? 'Name must have at least 2 letters' : null),
      email: (value) => (/^\S+@\S+$/.test(value) ? null : 'Invalid email'),
      age: (value) => (value < 18 ? 'You must be at least 18 to register' : null),
    },
  });

  const [componentsList, setComponentsList] = React.useState<idxComponentDf[]>([]);
  // componentsList used for saved all component use in this form builder

  const [componentFormBuilder, setComponentFormBuilder] = React.useState<idxComponentDf[]>([]);
  // build all component based on user created

  const handleAddComponent = () => {
    const components_list = [...componentsList];
    const component_form_builder = [...componentFormBuilder];
    const component_length = components_list.length
    const newComponent = {id : generateUniqueId(), field_type : 'TextInput', field_name : 'field_label_'+component_length, field_label : 'Field Label '+component_length}
    components_list.push(newComponent)
    setComponentsList(components_list)
    setComponentFormBuilder(component_form_builder)
    selectComponent(newComponent);
  }

  const handleSelectComponent = (id_component? : string) => {
    const components_list = [...componentsList];
    if(id_component){
      const find_component = components_list.find(comp => comp.id === id_component)
      console.log("handleSelectComponent", id_component)
      if(find_component){
        selectComponent(find_component)
      }
    }
  }

  return (
    <React.Fragment>
        <CardPaper>
          <CardPaperHeader>
            <CardPaperTitle>Dynamic Content</CardPaperTitle>
          </CardPaperHeader>
            <CardPaperBody>
              <Grid>
                <Grid.Col span={12}>
                  {componentsList.map((item, idx) => 
                    (item.field_type && item.id) && (
                      <DynamicComponentItem key={item.id} item={item} onClickSelect={handleSelectComponent}/>
                    )
                  )}
                </Grid.Col>
              </Grid>
              <Grid>
                <Grid.Col span={12}>
                  <Button onClick={handleAddComponent} fullWidth variant="light" radius="xs">Add Compnent</Button>
                </Grid.Col>
              </Grid>
            </CardPaperBody>
          </CardPaper>
    </React.Fragment>
  )
}

export default DynamicContentPageCreationTest